import express from 'express'
import http from 'http'
import path from 'path'
import * as socketio from 'socket.io'
import formatMessage from './utils/messages.js'
import {
  getCurrentUser,
  userJoin,
  userLeave,
  getRoomUser,
} from './utils/users.js'

const app = express()
const server = http.createServer(app)
const io = new socketio.Server(server)

app.use(express.json())

//Set static folder

app.use(express.static(path.join(path.resolve(), 'public')))

//run when clint connects
const botName = 'ChatCord Bot'

io.on('connection', (socket) => {
  socket.on('joinRoom', ({ username, room }) => {
    const user = userJoin(socket.id, username, room)

    socket.join(user.room)
    //Welcome current user
    socket.emit('message', formatMessage(botName, 'Welcome to ChatCord!'))

    //Broadcast when a user connects

    socket.broadcast
      .to(user.room)
      .emit(
        'message',
        formatMessage(botName, `${user.username} has joined the chat`)
      )

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUser(user.room),
    })
  })
  //Listen for chatMessage
  socket.on('chatMessage', (msg) => {
    const user = getCurrentUser(socket.id)
    io.to(user.room).emit('message', formatMessage(user.username, msg))
  })

  //Runs when client disconnects
  socket.on('disconnect', () => {
    const user = userLeave(socket.id)

    if (user) {
      io.to(user.room).emit(
        'message',
        formatMessage(botName, `${user.username} has left the chat`)
      )
    }

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUser(user.room),
    })
  })
})

const PORT = process.env.PORT || 3000

server.listen(PORT, () => {
  console.log(`Server Started At Port ${PORT}`)
})
